import './App.css';
import React from 'react';
import Input from './components/Input';
import ClearButton from './components/ClearButton';
import Button from './components/Button';
import { evaluate } from 'mathjs';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dataInput: "0",
    };
  }

  inputToAdd = (value) => {
    if (this.state.dataInput === "0") {
      this.setState({ dataInput: (this.state.dataInput = value) });
    } else {
      this.setState({ dataInput: (this.state.dataInput + value) });
    }
  }

  handleEqual = () => {
    this.setState({ dataInput: evaluate((this.state.dataInput).replaceAll('÷', '/')) });
  }

  handleSign = () => {
    this.setState({ dataInput: evaluate(this.state.dataInput + '*' + '-1') });
  }

  handlePercentage = () => {
    this.setState({ dataInput: evaluate(this.state.dataInput + "%") });
  }

  render() {
    return (
      <div className='calculator-app'>

        <h1>
          Calculator using ReactJS
        </h1>

        <div className='calculator-container'>
          <Input input={this.state.dataInput} />

          <div className='row'>
            <ClearButton handleClear={() => this.setState({ dataInput: "0" })}>
              AC
            </ClearButton>
            <Button handleClick={() => this.handleSign()}>-/+</Button>
            <Button handleClick={() => this.handlePercentage()}>%</Button>
            <Button handleClick={this.inputToAdd}>÷</Button>
          </div>

          <div className='row'>
            <Button handleClick={this.inputToAdd}>7</Button>
            <Button handleClick={this.inputToAdd}>8</Button>
            <Button handleClick={this.inputToAdd}>9</Button>
            <Button handleClick={this.inputToAdd}>*</Button>
          </div>

          <div className='row'>
            <Button handleClick={this.inputToAdd}>4</Button>
            <Button handleClick={this.inputToAdd}>5</Button>
            <Button handleClick={this.inputToAdd}>6</Button>
            <Button handleClick={this.inputToAdd}>-</Button>
          </div>

          <div className='row'>
            <Button handleClick={this.inputToAdd}>1</Button>
            <Button handleClick={this.inputToAdd}>2</Button>
            <Button handleClick={this.inputToAdd}>3</Button>
            <Button handleClick={this.inputToAdd}>+</Button>
          </div>

          <div className='last-row'>
            <Button handleClick={this.inputToAdd}>0</Button>
            <div className='last-column'>
              <Button handleClick={this.inputToAdd}>.</Button>
              <Button handleClick={() => this.handleEqual()}>=</Button>
            </div>
          </div>

        </div>
      </div>
    );
  }
}

export default App;
